import React from "react";
import Products from "../../componets/Products";
import LogoTags from "../../componets/LogoTags";
import Blog from "../../componets/Blog";

const HomePage = ({ ...otherProps }) => {
  return (
    <>
      <Products />
      <LogoTags />
      <Blog />
    </>
  );
};
export default HomePage;
