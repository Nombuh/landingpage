import React from "react";
import Header from "../../componets/Header";
import Footer from "../../componets/Footer";
import Hero from "../../assets/56256.png";
const LandingPageLayout = ({ ...otherProps }) => {
  return (
    <section className="xl:mx-20">
      <Header />
      <div className="mt-36 relative bg-white overflow-hidden">
        <div className="mt-10 max-w-7xl mx-auto">
          <div className="mt-10 relative z-0  bg-white sm:pb-16 md:pb-20 lg:max-w-2xl lg:w-full lg:pb-52 xl:pb-32">
            <div className="mt-10 mx-auto max-w-7xl sm:mt-12 sm:px-6 md:mt-16 lg:mt-20 lg:px-8 xl:mt-28">
              <div className="ml-12 sm:text-center lg:text-left">
                <h1 className="text-xl tracking-tight font-medium text-gray-500 sm:text-5xl md:text-4xl">
                  <span className="block xl:inline">Start new... Today!</span>{" "}
                </h1>
                <p className="mt-3 text-base text-gray-500 sm:mt-5 sm:text-lg sm:max-w-xl sm:mx-auto md:mt-5 md:text-xl lg:mx-0">
                  Vivamus vestibulum elit efficitur, elementum sapien a, aliquet
                  ipsum
                </p>
              </div>
            </div>
          </div>
        </div>
        <div className="md:absolute md:inset-y-0 md:right-0 md:w-1/2 mt-8 sm:ml-10">
          <img className="ml-12 z-10 object-none h-auto" src={Hero} alt="hero" />
        </div>
      </div>
      <div className="w-full max-w-7xl mx-auto">
        {otherProps.children}
      </div>
      
      <Footer/>
    </section>
  );
};

export default LandingPageLayout;
