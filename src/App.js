import React from 'react'
import { Switch, Route } from 'react-router-dom';

import LandingPageLayout from './layouts/LandingPage';
import HomePage from './pages/HomePage';
import Blog from './componets/Blog';
import Header from './componets/Header';
import ContactsCard from './componets/Contacts';
import PageNotFound from './componets/PageNotFound';

const App =()=> {
  return (
    <Switch>
      <Route path="/" exact>
        <LandingPageLayout>
          <HomePage />
        </LandingPageLayout>
      </Route>
      <Route path="/Products">
        <Header />
        <Blog />
      </Route>
      <Route path="/Blog">
        <LandingPageLayout>
          <HomePage />
        </LandingPageLayout>
      </Route>
      <Route path="/Contacts">
        <Header />
        <ContactsCard />
      </Route>
      <Route path="/*"><PageNotFound/></Route>
    </Switch>
  );
}

export default App;
