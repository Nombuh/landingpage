import React from "react";
import Image from "../../assets/56217.png";

const Products = () => {
  return (
    <section className="m:py-12 bg-white">
      <div className="max-w-7xl mx-auto px-4 sm:px-2 md:px-8 ">
        <p className="m-12 md:text-center text-2xl leading-8 font-bold tracking-tight text-gray-500 md:text-4xl">
          Lorem ipsum something
        </p>
      </div>
      <div className="relative bg-white">
        <div className="max-w-7xl mx-auto ">
          <div className="relative z-0 md:pb-8 bg-white ">
            <main className="mt-0 mx-auto max-w-7xl px-4 sm:mt-12 sm:px-6 md:mt-16 lg:mt-20 lg:px-8 xl:mt-28">
              <div className="hidden ml-24 md:block">
                <img src={Image} alt="new product" />
              </div>
            </main>
          </div>
        </div>
        <div className="m-16 md:mt-20 md:absolute md:inset-y-0 md:right-0 md:mr-24">
          <h3 className="sm:max-w-xl sm:mx-auto text-xl font-medium text-gray-600">
            New Product,new Story
          </h3>

          <p className="mt-3 text-base text-gray-500 sm:mt-5 sm:text-lg sm:max-w-xl sm:mx-auto md:mt-5 md:text-xl lg:mx-0">
            Vivamus vestilubum elit efficitur, elementum sapien a, aliquet
            ipsum.Fusce placerat dolor id cursus finibus. Aliquam tempus
            facilisis ipsum sit amet molestie.Proin lobortis eros a turpis
            tempor,sed ornare augue aliquam. Donec imperdiet nulla ut placerat
            molestie. In hendreit blandit ante facilisis ultrices. Mauris
            vulputate metus sit amet ex dignissim,sed hendrerit nunc rhoncus.
          </p>
        </div>
      </div>
    </section>
  );
};

export default Products;
