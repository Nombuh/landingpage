import React from "react";
import FooterLogo from '../../assets/FooterLogo.png';

const Footer = ({ ...otherProps }) => {
    return (
      <footer className="bg-blue-500">
        <div className="max-w-7xl mx-auto px-2 sm:px-4 md:px-8">
          <div className="ml:36 md:text-xl px-20 flex text-gray-50 py-3 ">
            <img src={FooterLogo} alt="" />
            <div className="px-8"> All rights reserved &copy; </div>
          </div>
        </div>
      </footer>
    );
};
export default Footer;
