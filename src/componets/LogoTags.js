import React from 'react'
import Logo from "../assets/Logo.png"

const LogoTags = () => {
  return (
    <div className="md:flex justify-evenly bg-gray-200 p-8">
      <div className="relative flex justify-evenly ">
        <div className="mr-6 ">
          <img src={Logo} alt="hash-logo" />
        </div>
        <div className="mr-6">
          <img src={Logo} alt="hash-logo" />
        </div>
        <div className="mr-6">
          <img src={Logo} alt="hash-logo" />
        </div>
        <div className="mr-6">
          <img src={Logo} alt="hash-logo" />
        </div>
        <div className="mr-6">
          <img src={Logo} alt="hash-logo" />
        </div>
      </div>
    </div>
  );
}

export default LogoTags;
