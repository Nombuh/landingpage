import React,{useState} from "react";
import Photo from "../../assets/dustin-lee-19667-unsplash.png";

const Blog = ({ ...otherProps }) => {

  const [textColor, setTextColor] = useState('#4285BC')
  const [isBlue,setIsBlue] =useState(true)

  const handleColorChange = () => {
    setIsBlue(!isBlue)
    setTextColor(isBlue ?'#FFFFFF': '#4285BC')
  }
  
  return (
    <section className="py-14 mx-8">
      <div className="mt-6 grid  grid-cols-1 gap-y-10 gap-x-6 sm:grid-cols-2 md:grid-cols-4 xl:gap-x-8">
        <div className="item-list">
          <div
            className="aspect-w-1 aspect-h-1 rounded-lg bg-white group-hover:opacity-75"
            onClick={handleColorChange}
          >
            <div>
              <img src={Photo} alt="blog" className="w-full" />
            </div>
            <div className="p-2.5">
              <ul>
                <li className="list-none">
                  <span className="font-bold text-base">Blog Title #1</span>
                </li>
                <li className="list-none">
                  <span className="text-base">
                    Blog excerpt-first lines-for approx two lines
                  </span>
                </li>
              </ul>
            </div>
          </div>
        </div>
        <div className="item-list">
          <div
            className="aspect-w-1 aspect-h-1 rounded-lg bg-white group-hover:opacity-75"
            onClick={handleColorChange}
          >
            <div>
              <img src={Photo} alt="blog" className="w-full" />
            </div>
            <div className="p-2.5">
              <ul>
                <li className="list-none">
                  <span className="font-bold text-base">Blog Title #1</span>
                </li>
                <li className="list-none">
                  <span className="text-base">
                    Blog excerpt-first lines-for approx two lines
                  </span>
                </li>
              </ul>
            </div>
          </div>
        </div>
        <div className="item-list">
          <div
            className="aspect-w-1 aspect-h-1 rounded-lg bg-white group-hover:opacity-75"
            onClick={handleColorChange}
          >
            <div>
              <img src={Photo} alt="blog" className="w-full" />
            </div>
            <div className="p-2.5">
              <ul>
                <li className="list-none">
                  <span className="font-bold text-base">Blog Title #1</span>
                </li>
                <li className="list-none">
                  <span className="text-base">
                    Blog excerpt-first lines-for approx two lines
                  </span>
                </li>
              </ul>
            </div>
          </div>
        </div>
        <div className="item-list">
          <div
            className="aspect-w-1 aspect-h-1 rounded-lg bg-white group-hover:opacity-75"
            onClick={handleColorChange}
          >
            <div>
              <img src={Photo} alt="blog" className="w-full" />
            </div>
            <div className="p-2.5">
              <ul>
                <li className="list-none">
                  <span className="font-bold text-base">Blog Title #1</span>
                </li>
                <li className="list-none">
                  <span className="text-base">
                    Blog excerpt-first lines-for approx two lines
                  </span>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
      <div class="btn-more ml-28 relative lg:ml-96 content-center rounded">
        {isBlue ? (
          <button
            style={{ color: textColor }}
            className="lg:ml-28 lg:inline-flex border border-blue-500 
            font-semibold  py-2 px-4 rounded-full"
          >
            Read More
          </button>
        ) : (
          <button
            className="lg:ml-28 lg:inline-flex bg-transperent bg-blue-500 
          font-semibold py-2 px-4 rounded-full text-white"
          >
            Read More
          </button>
        )}
      </div>
    </section>
  );
};
export default Blog;
